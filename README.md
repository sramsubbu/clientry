Clientry
===========


Introduction
-------------
Swagger for CLI

Define your command line arguments in a YAML file and let clientry
parse the arguments for you.



Installation and Usage
----------------------
<docs>


Bugs
-----
For any bugs please contact the author Ramasubramanian S<sramsubu@gmail.com>
or raise an issue in the issue tracker


Changelog
----------
List of changes can be found in [Changelog](CHANGELOG.md)
