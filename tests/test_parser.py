import pytest
from clientry.parser import parse_instance
from jsonschema import ValidationError, SchemaError


def test_parser():
    instance = {
        "version": "v1",
        "program_name": "my_app",
        "program_version": "0.1.0",
        "program": {
            "single_command": True
        }
    }
    assert parse_instance(instance)


def test_parser_missing_required_field():
    error_instance = {
        "version": "v1",
        "program_name": "my_app",
        "program": {
            "single_command": True
        },
        "short_description": 'Some dummy app to test validation'
    }

    with pytest.raises(ValidationError):
        parse_instance(error_instance)


def test_parser_optional():
    program = {
        "single_command": True,
        "command": {
            "name": "run",
            "entry_point": "app.app:main",
            "short_description": "simple test cmd"
        }
    }
    instance = {
        "version": "v1",
        "program_name": "my_app",
        "program_version": "0.1.0",
    }
    instance['program'] = program
    parse_instance(instance)

    program['short_description'] = 'Sample command'
    parse_instance(instance)


def test_parser_with_invalid_version():
    instance = {
        "version": "v0",
        "program_name": "app",
        "program_version": "0.1.0",
        "program": {
            "single_command": True
        }
    }
    with pytest.raises(SchemaError):
        parse_instance(instance)
