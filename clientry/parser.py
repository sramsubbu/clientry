import jsonschema
import json
from pathlib import Path

SCHEMA_PATH = Path(__file__).parent.parent / 'schemas'


def load_schema(version="v1"):
    schema_path = SCHEMA_PATH / f"{version}.json"
    if not schema_path.exists():
        raise jsonschema.SchemaError("schema v0 does not exist")
    with open(schema_path) as f:
        return json.load(f)


def parse_instance(instance):
    version = instance['version']
    schema = load_schema(version)
    jsonschema.validate(schema=schema, instance=instance)
    return instance


